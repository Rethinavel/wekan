package com.wekan.api.utils.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar


/** apply title to the toolbar simply. */
fun AppCompatActivity.initToolbar(toolbar: Toolbar, title: String = "", hasBackButton: Boolean) {
    toolbar.title = title
    setSupportActionBar(toolbar)
    supportActionBar!!.setDisplayHomeAsUpEnabled(hasBackButton)
}

// Un comment the following  if you want to transform the animation...

/*
*
*
*
*
*
*





*/
/** get a material container arc transform. *//*

fun AppCompatActivity.getContentTransform(): MaterialContainerTransform {
    return MaterialContainerTransform(this).apply {
        addTarget(android.R.id.content)
        duration = 400
        pathMotion = MaterialArcMotion()
    }
}

*/
/** apply material exit container transformation. *//*

fun AppCompatActivity.applyExitMaterialTransform() {
    window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
    setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
    window.sharedElementsUseOverlay = false
}

*/
/** apply material entered container transformation. *//*

fun AppCompatActivity.applyMaterialTransform(transitionName: String) {
    window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
    ViewCompat.setTransitionName(findViewById<View>(android.R.id.content), transitionName)

    // set up shared element transition
    setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
    setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
    window.sharedElementEnterTransition = getContentTransform()
    window.sharedElementReturnTransition = getContentTransform()
}
*/
