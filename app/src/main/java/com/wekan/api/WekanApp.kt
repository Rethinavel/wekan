package com.wekan.api

import com.wekan.api.di.component.DaggerAppComponent
import dagger.android.DaggerApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import timber.log.Timber

@Suppress("unused")
class WekanApp : DaggerApplication() {

    private val appComponent = DaggerAppComponent.factory().create(this)

    override fun onCreate() {
        super.onCreate()

        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("Antaro.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                ).build()
        )

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
        else {
            // Timber.plant(CrashReportingTree())  todo later..
        }
    }

    override fun applicationInjector() = appComponent

}

