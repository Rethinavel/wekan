package com.wekan.api.viewmodel.webservice

import com.wekan.api.model.Station
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {

    @GET("v1.json")
    fun getStations(
        @Query("api_key") DEMO_KEY: String
        , @Query("fuel_type") fuel_type: String
        , @Query("state") state: String
        , @Query("limit") limit: Int
    ): Observable<Station>
}