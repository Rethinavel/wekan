package com.wekan.api.viewmodel.repository

import com.wekan.api.model.Station
import com.wekan.api.viewmodel.webservice.ApiService
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class StationRepository @Inject constructor(private val apiService: ApiService) {

    fun getStationList(): Observable<Station> {
        return apiService.getStations("DEMO_KEY", "E85,ELEC", "CA", 100)
    }
}

//
//class StationRepository {
//
//    private var mApiRequest: ApiService? = null
//
//    init {
//        mApiRequest = ApiService.Factory.create()
//    }
//
//    companion object {
//        private var stationRepository: StationRepository? = null
//
//        @Synchronized
//        @JvmStatic
//        fun getInstance(): StationRepository {
//            if (stationRepository == null)
//                stationRepository = StationRepository()
//            return stationRepository!!
//        }
//    }
//
//    fun getStationList(): MutableLiveData<Station> {
//        // As of now.. We pass the data statically..
//        val data = MutableLiveData<Station>()
//        mApiRequest?.getStations("DEMO_KEY", "E85,ELEC", "CA", 100)
//            ?.enqueue(object : Callback<Station> {
//                override fun onResponse(
//                    call: Call<Station>, response: Response<Station>
//                ) {
//                    data.value = response.body()
//                }
//
//                override fun onFailure(call: Call<Station>, t: Throwable) {
//                    data.value = null
//                }
//            })
//        return data
//    }
//}





