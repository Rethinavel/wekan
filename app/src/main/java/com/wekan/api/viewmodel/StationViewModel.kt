package com.wekan.api.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.wekan.api.utils.Result
import com.wekan.api.utils.defaultErrorHandler
import com.wekan.api.viewmodel.repository.StationRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


//
//class StationViewModel(application: Application) : AndroidViewModel(application) {
//
//    private val stationListObservable = StationRepository.getInstance()
//        .getStationList()
//
//    fun getStationObservable(): MutableLiveData<Station> {
//        return stationListObservable
//    }
//}


class StationViewModel @Inject constructor(private val repository: StationRepository) : ViewModel(),
    LifecycleObserver {

    private val disposable = CompositeDisposable()

    val result = repository.getStationList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .map { it }
        .doOnError(defaultErrorHandler())
        .onErrorReturn { null }
       // .startWith(Result.loading())


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
