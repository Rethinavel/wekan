package com.wekan.api.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.wekan.api.R
import com.wekan.api.databinding.AdapterStationListBinding
import com.wekan.api.model.FuelStation
import java.util.*


class StationAdapter(private val stationList: MutableList<FuelStation>) :
    RecyclerView.Adapter<StationAdapter.StationListViewHolder>() {

    var tempList: MutableList<FuelStation>? = null

    init {
        tempList = mutableListOf()
        tempList!!.addAll(stationList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationListViewHolder {
        val binding = DataBindingUtil.inflate<AdapterStationListBinding>(
            LayoutInflater.from(parent.context), R.layout.adapter_station_list,
            parent, false
        )
        return StationListViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return stationList.size
    }

    override fun onBindViewHolder(holder: StationListViewHolder, position: Int) {
        holder.binding.data = stationList.get(position)
        holder.binding.executePendingBindings()
    }

    class StationListViewHolder(val binding: AdapterStationListBinding) :
        RecyclerView.ViewHolder(binding.root)


    fun filter(charText: String) {
        val char = charText.toLowerCase(Locale.getDefault())
        stationList.clear()
        if (char.length == 0)
            stationList.addAll(tempList!!.toList().toMutableList())
        else {
            for (mData in tempList!!) {
                if (mData.station_name.toLowerCase(Locale.getDefault()).contains(char))
                    stationList.add(mData)
            }
        }
        notifyDataSetChanged()
    }

}
