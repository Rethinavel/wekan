package com.wekan.api.view.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.wekan.api.base.BaseActivity
import com.wekan.api.utils.ConnectivityMonitor

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ConnectivityMonitor.getInstance(this).startListening {
            if (it)
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        }
    }
}