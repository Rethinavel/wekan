package com.wekan.api.view.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.wekan.api.R
import com.wekan.api.base.BaseActivity
import com.wekan.api.databinding.ActivityMainBinding
import com.wekan.api.model.FuelStation
import com.wekan.api.utils.extensions.initToolbar
import com.wekan.api.view.adapter.StationAdapter
import com.wekan.api.viewmodel.StationViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), SearchView.OnQueryTextListener {

    private var mSearchView: SearchView? = null
    private lateinit var stationAdapter: StationAdapter
    private var stationList = mutableListOf<FuelStation>()
    private val stationViewModel: StationViewModel by injectViewModels()
    private val binding: ActivityMainBinding by binding(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initToolbar(binding.toolbar, getString(R.string.title_station), true)

        val recyclerLayoutManager = LinearLayoutManager(this)
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = recyclerLayoutManager
        }

        stationViewModel.result.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribe {

                if (it != null) {
                    txtLoading.visibility = View.GONE
                    stationList.clear()
                    stationList.addAll(it.fuel_stations)
                    stationAdapter = StationAdapter(stationList)
                    recyclerView.adapter = stationAdapter
                    stationAdapter.notifyDataSetChanged()
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_student_list, menu)
        val searchMenuItem = menu?.findItem(R.id.ic_menu_action_serach)
        searchMenuItem!!.isVisible = true
        mSearchView = searchMenuItem.actionView as SearchView
        mSearchView?.queryHint = getString(R.string.action_menu_search)
        mSearchView?.setOnQueryTextListener(this)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        stationAdapter.filter(p0!!)
        return false
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }
}


//        viewModel.getStationObservable().observe(this, Observer<Station> { mItems ->
//            if (mItems != null && mItems.fuel_stations.isNotEmpty()) {
//                txtLoading.visibility = View.GONE
//                stationList.clear()
//                stationList.addAll(mItems.fuel_stations)
//                stationAdapter = StationAdapter(stationList)
//                recyclerView.adapter = stationAdapter
//                stationAdapter.notifyDataSetChanged()
//            } else
//                txtLoading.text = getString(R.string.error_message_unable_to_reach_server)
//        })
