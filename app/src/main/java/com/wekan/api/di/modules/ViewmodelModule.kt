package com.wekan.api.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.wekan.api.di.annotations.ViewModelKey
import com.wekan.api.di.factory.AppViewModelFactory
import com.wekan.api.viewmodel.StationViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(StationViewModel::class)
    abstract fun binsStationListViewModel(viewModel: StationViewModel): ViewModel


//    @Binds
//    @IntoMap
//    @ViewModelKey(SplashViewModel::class)
//    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel
//
//    @Binds
//    @IntoMap
//    @ViewModelKey(DetailsViewModel::class)
//    internal abstract fun bindSplashViewModel(viewModel: DetailsViewModel): ViewModel
}