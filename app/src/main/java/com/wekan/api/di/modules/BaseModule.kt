
package com.wekan.api.di.modules


import com.wekan.api.base.BaseActivity
import com.wekan.api.base.BaseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class BaseModule {

  @ContributesAndroidInjector
  internal abstract fun contributeViewModelActivity(): BaseActivity

  @ContributesAndroidInjector
  internal abstract fun contributeViewModelFragment(): BaseFragment
}
