

package com.wekan.api.di.modules


import com.wekan.api.di.annotations.ActivityScope
import com.wekan.api.view.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector( )
    internal abstract fun contributeMainActivity(): MainActivity
}


/*
*
*
*

As of now, Add you activities here.. I will update later.

*
*
*
* */


//@ActivityScope
//@ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
//internal abstract fun contributeMainActivity(): MainActivity


//  @ActivityScope
//  @ContributesAndroidInjector
//  internal abstract fun contributeMovieDetailActivity(): MovieDetailActivity
//
//  @ActivityScope
//  @ContributesAndroidInjector
//  internal abstract fun contributeTvDetailActivity(): TvDetailActivity
//
//  @ActivityScope
//  @ContributesAndroidInjector
//  internal abstract fun contributePersonDetailActivity(): PersonDetailActivity

