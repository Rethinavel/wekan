package com.wekan.api.di.modules

import android.content.Context
import com.wekan.api.di.annotations.qualifier.ApplicationContext
import com.wekan.api.di.annotations.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ContextModule(private val context: Context) {
    @Provides
    @ApplicationScope
    @ApplicationContext
    fun provideContext(): Context {
        return context
    }

}