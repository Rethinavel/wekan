package com.wekan.api.di.component

import android.app.Application
import com.wekan.api.di.annotations.ApplicationScope
import com.wekan.api.di.modules.ActivityModule
import com.wekan.api.di.modules.BaseModule
import com.wekan.api.di.modules.NetworkModule
import com.wekan.api.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Singleton


@ApplicationScope
@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        ActivityModule::class,
        BaseModule::class,
        ViewModelModule::class,
        NetworkModule::class]
)

// Add PersistenceModule Module when it is necessary | if you want to play with the Room database library..
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    override fun inject(instance: DaggerApplication)
}