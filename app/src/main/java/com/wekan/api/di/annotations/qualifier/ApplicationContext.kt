package com.wekan.api.di.annotations.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext
