package com.wekan.api.model

data class Station(
    val fuel_stations: List<FuelStation>,
    val station_counts: StationCounts,
    val station_locator_url: String,
    val total_results: Int
)

data class FuelStation(
    val access_code: String,
    val access_days_time: String,
    val access_days_time_fr: Any,
    val access_detail_code: String,
    val bd_blends: Any,
    val bd_blends_fr: Any,
    val cards_accepted: String,
    val city: String,
    val cng_dispenser_num: Any,
    val cng_fill_type_code: Any,
    val cng_psi: Any,
    val cng_renewable_source: Any,
    val cng_total_compression: Any,
    val cng_total_storage: Any,
    val cng_vehicle_class: Any,
    val country: String,
    val date_last_confirmed: String,
    val e85_blender_pump: Boolean,
    val e85_other_ethanol_blends: Any,
    val ev_connector_types: List<String>,
    val ev_dc_fast_num: Int,
    val ev_level1_evse_num: Int,
    val ev_level2_evse_num: Int,
    val ev_network: String,
    val ev_network_ids: EvNetworkIds,
    val ev_network_web: String,
    val ev_other_evse: String,
    val ev_pricing: String,
    val ev_pricing_fr: Any,
    val ev_renewable_source: String,
    val expected_date: Any,
    val facility_type: String,
    val fuel_type_code: String,
    val geocode_status: String,
    val groups_with_access_code: String,
    val groups_with_access_code_fr: String,
    val hy_is_retail: Any,
    val hy_pressures: Any,
    val hy_standards: Any,
    val hy_status_link: Any,
    val id: Int,
    val intersection_directions: String,
    val intersection_directions_fr: Any,
    val latitude: Double,
    val lng_renewable_source: Any,
    val lng_vehicle_class: Any,
    val longitude: Double,
    val lpg_nozzle_types: Any,
    val lpg_primary: Any,
    val ng_fill_type_code: Any,
    val ng_psi: Any,
    val ng_vehicle_class: Any,
    val open_date: String,
    val owner_type_code: String,
    val plus4: Any,
    val state: String,
    val station_name: String,
    val station_phone: String,
    val status_code: String,
    val street_address: String,
    val updated_at: String,
    val zip: String
)

data class EvNetworkIds(
    val ports: List<String>,
    val posts: List<String>
)

data class StationCounts(
    val fuels: Fuels,
    val total: Int
)

data class Fuels(
    val BD: BD,
    val CNG: CNG,
    val E85: E85,
    val ELEC: ELEC,
    val HY: HY,
    val LNG: LNG,
    val LPG: LPG
)

data class BD(
    val total: Int
)

data class CNG(
    val total: Int
)

data class E85(
    val total: Int
)

data class ELEC(
    val stations: Stations,
    val total: Int
)

data class Stations(
    val total: Int
)

data class HY(
    val total: Int
)

data class LNG(
    val total: Int
)

data class LPG(
    val total: Int
)