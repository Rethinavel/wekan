const val kotlinVersion = "1.3.61"

object BuildPlugins {

    object Versions {
        const val androidBuildToolsVersion = "3.5.3"
    }

    const val androidGradlePlugin =
        "com.android.tools.build:gradle:${Versions.androidBuildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"

    const val androidApplication = "com.android.application"
    const val androidLibrary = "com.android.library"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
    const val kotlinKapt = "kotlin-kapt"
}

object AppVersions {
    const val versionCode = 1
    const val versionName = "1.0"
}


object AndroidSdk {
    const val min = 16
    const val compile = 29
    const val target = 29
    const val buildToolsVersion = "29.0.3"
}

object Versions {

    const val androidx = "1.1.0"
    const val constraintLayout = "1.1.3"
    const val architecture_components_version = "2.1.0"

    const val retrofit = "2.6.2"
    const val retrofitLoggingInterceptor = "4.2.2"
    const val retrofitAdapter = "2.4.0"

    const val calligrayphy = "3.1.1"
    const val viewpump = "2.0.3"
    const val timber = "4.7.1"

    const val daggerVersion = "2.26"
    const val rxJava = "2.1.10"  // "3.0.0"
    const val rxKotlin = "2.4.0"
    const val rxAndroid = "2.0.2"

    const val multidexVersion = "1.0.3"

    const val activityKtxVersion = "1.1.0"
    const val fragmentKtxVersion = "1.2.2"

    const val glideVersion = "4.11.0"
}

object Deps {

    const val kotlinStandardLibrary = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${kotlinVersion}"
    const val ktx = "androidx.core:core-ktx:${Versions.androidx}"
    const val multidex = "com.android.support:multidex:${Versions.multidexVersion}"

    const val appCompat = "androidx.appcompat:appcompat:${Versions.androidx}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.androidx}"
    const val material = "com.google.android.material:material:1.1.0-alpha05"


    // Android lifecycle architecture components..
    const val viewmodelArchComponent =
        "androidx.lifecycle:lifecycle-extensions:${Versions.architecture_components_version}"
    const val lifecycleCompiler =
        "androidx.lifecycle:lifecycle-compiler:${Versions.architecture_components_version}"

    const val activityKTX = "androidx.activity:activity-ktx:${Versions.activityKtxVersion}"
    const val fragmentKTX = "androidx.fragment:fragment-ktx:${Versions.fragmentKtxVersion}"

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitConverterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofitLoggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.retrofitLoggingInterceptor}"
    const val retrofitAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitAdapter}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glideVersion}"

    const val calligrayphy = "io.github.inflationx:calligraphy3:${Versions.calligrayphy}"
    const val viewpump = "io.github.inflationx:viewpump:${Versions.viewpump}"
    const val timberLogging = "com.jakewharton.timber:timber:${Versions.timber}"

    // Dagger

    const val dagger = "com.google.dagger:dagger:${Versions.daggerVersion}"
    const val daggerAndroid = "com.google.dagger:dagger-android:${Versions.daggerVersion}"
    const val daggerAndroidSupport =
        "com.google.dagger:dagger-android-support:${Versions.daggerVersion}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.daggerVersion}"
    const val daggerAnnotationProcessor =
        "com.google.dagger:dagger-android-processor:${Versions.daggerVersion}"


    // RxJava/Android
    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
}


object TestLibraries {

    private object Versions {
        const val junit4 = "4.13-beta-2"
        const val espresso = "3.3.0-alpha03"
        const val espressoTestRules = "1.3.0-alpha03"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val espressoTestRules = "androidx.test:rules:${Versions.espressoTestRules}"
}

